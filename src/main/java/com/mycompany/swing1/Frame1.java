/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swing1;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Aritsu
 */
public class Frame1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1280,768);
        JLabel lbHelloWorld = new JLabel("HelloWorld!!",JLabel.CENTER);
        lbHelloWorld.setBackground(Color.green);
        lbHelloWorld.setOpaque(true);
        lbHelloWorld.setFont(new Font("Verdana",Font.PLAIN,18));
        
        frame.add(lbHelloWorld);
        frame.setVisible(true);
    }
}
